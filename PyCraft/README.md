**Protocole d’utilisation (windows):**


1. Assurez-vous d’avoir la version Python 3.10, 3.11 (ou 3.12), si ce n’est pas le cas, il faut l’installer
2. Copiez le dossier du jeu à un endroit où l’écriture disque est autorisée mais également rapide (pas sur clé USB, pas sur serveur réseau)


Installation des librairies :

3. Allez à l'interieur du dossier PyCraft, puis ouvrez une invite de commande (cmd) à cet endroit (il faut cliquer sur la partie droite de la barre de chemin puis entrer "cmd").

4. Installez les librairies en executant la commande suivante : "pip install -r required_libs.txt"

Erreurs possibles pour l'étape précédente : 
- si vous avez une erreur de “pip non trouvé” sur l’étape précédente, il faut le remplacer par : “chemin/vers/le/pip.exe” install –r required_libs.txt (le pip.exe se trouve généralement dans le dossier Scripts de l’installation python)
- si vous avez une erreur d'autorisations / privilèges (vous n'avez pas assez de droits pour installer les librairies), vous pouvez essayer d'executer le terminal en administrateur ou bien avec la commande : "pip install --user -r required_libs.txt"


Verifications avant le lancement du jeu :

5. Ensuite, assurez-vous d’avoir une quantité d’espace de stockage disponible suffisante (il faut compter environ 200 Mb pour un grand monde, prévoyez de la marge surtout si vous comptez faire plusieurs mondes)

6. Si jamais vous souhaitez modifier certains paramètres par défaut du jeu, vous pouvez aller dans le fichier “sources/preferences.txt” et choisir vos préférences (telles que la sensibilité de la souris ou la renderdistance). Il est conseillé de prendre connaissance des informations dans preferences.txt, pour avoir une idée des contrôles du jeu (comment bouger, mettre en pause, quitter, etc.).
Sachez juste au moins qu'il faut appuyer sur "echap" pour libérer la souris.


Lancement du jeu :

7. Ouvrez une invite de commande (cmd) dans le repertoire du jeu : 
- Si vous êtes dans un cmd et dans le repertoire où vous avez executé le "pip install" (étape 4), tapez la commande "cd sources"
- Sinon, allez dans le repertoire PyCraft/sources, puis comme précedement, ouvrez une invite de commande à cet endroit (il faut cliquer sur la partie droite de la barre de chemin puis entrer "cmd").

8. Dans l’invite de commande (qui se situe donc à PyCraft/sources), exécutez la commande “python main.py” pour lancer le jeu
Remarque : A noter que si vous avez une erreur de “python non trouvé” sur l’étape précédente, vous pouvez essayer de :
remplacer "python" par : “chemin/vers/le/python.exe” main.py (le python.exe se trouve géneralement directement dans le dossier de l’installation python)


9. Suivez ensuite les instructions qui apparaitront dans l’invite de commande (pressez c pour créer un monde et o pour un ouvrir un)
10. Le jeu se lance ensuite !

(11.)	En cas d'erreur, veuillez vous tourner vers la section sur les erreurs dans le guide utilisateur de la documentation trouvable à doc/documentation.pdf

Pour relancer le jeu, recommencez à l'étape 8 en re-executant juste la commande dans le cmd

Pour supprimer tous les mondes que vous avez créé, executez le fichier "reboot.bat" (qui execute reboot.py). Si il ne marche pas, executez le fichier "reboot.py" de la meme manière que vous executez le fichier "main.py" pour lancer le jeu (dans la commande, remplacez "main.py" par "reboot.py").\\
On vous demandera alors de confirmer, pressez "o", et la suppression commencera.